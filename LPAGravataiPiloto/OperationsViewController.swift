//
//  ViewController.swift
//  LPAGravataiPiloto
//
//  Created by Felix Canaparro on 02/12/2017.
//  Copyright © 2017 AndroidFelixGravatai. All rights reserved.
//
import  Foundation
import UIKit
import MessageUI


class OperationsViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var textNomePA: UITextField!
    @IBOutlet weak var textDataPA: UITextField!
    @IBOutlet weak var textNomeSA: UITextField!
    @IBOutlet weak var textDataSA: UITextField!
    @IBOutlet weak var textEstacao: UITextField!
    @IBOutlet weak var textLinha: UITextField!
    @IBOutlet weak var questionsTableVIew: UITableView!
    @IBOutlet weak var primNivelRespUm: UISegmentedControl!
    @IBOutlet weak var primNivelRespDois: UISegmentedControl!
    @IBOutlet weak var primNivelRespTres: UISegmentedControl!
    @IBOutlet weak var primNivelRespQuatro: UISegmentedControl!
    @IBOutlet weak var primNivelRespOito: UISegmentedControl!
    @IBOutlet weak var primNivelRespSete: UISegmentedControl!
    @IBOutlet weak var primNivelRespSeis: UISegmentedControl!
    @IBOutlet weak var primNivelRespCinco: UISegmentedControl!
    @IBOutlet weak var segNivelRespTres: UISegmentedControl!
    @IBOutlet weak var segNivelRespQuatro: UISegmentedControl!
    @IBOutlet weak var segNivelRespSeis: UISegmentedControl!
    @IBOutlet weak var segNivelRespSete: UISegmentedControl!
    @IBOutlet weak var segNivelRespOito: UISegmentedControl!
    @IBOutlet weak var segNivelRespCinco: UISegmentedControl!
    @IBOutlet weak var segNivelRespDois: UISegmentedControl!
    @IBOutlet weak var PrimNivelRespNove: UISegmentedControl!
    @IBOutlet weak var primNivelRespDez: UISegmentedControl!
    @IBOutlet weak var segNivelRespDez: UISegmentedControl!
    @IBOutlet weak var segNivelRespNove: UISegmentedControl!
    @IBOutlet weak var segNivelRespOnze: UISegmentedControl!
    @IBOutlet weak var primNivelRespOnze: UISegmentedControl!
    @IBOutlet weak var primNivelRespDoze: UISegmentedControl!
    @IBOutlet weak var segNivelRespDoze: UISegmentedControl!
    
    @IBOutlet weak var scrol: UIScrollView!
    
    @IBOutlet weak var afqTableView: UITableView!
    
    @IBOutlet weak var switchSeg: UISwitch!
    @IBOutlet weak var switchPrim: UISwitch!
    @IBOutlet weak var segNivelRespUm: UISegmentedControl!
    @IBOutlet weak var textInputUm: UITextField!
    
    @IBOutlet weak var textInputDois: UITextField!
    
    @IBOutlet weak var textInputTres: UITextField!
    @IBOutlet weak var textInputQuatro: UITextField!
    
    
    let LpaTxtDelegate=LPATextFieldDelegate()
    let questionsArray = [
        "TM esta usando Epi's?",
        "TM mantendo o 5S??",
        "TM apto conforme matriz de versatilidade?",
        "TM seguindo SWS?",
        "Possui alerta de qualideade?Operador treinado? ( Se aplicavel) ",
        "PVS sendo executado? ( Se aplicavel)",
        "Ferramentas da estacao/dispositivos estao calibrados? ( Se aplicavel)",
        "PokaYoke revisado? ( Se aplicavel)",
        ]
    let VFQArray=["QFA sendo realizado e conforme (pasta no mural da linha 'QFA e ZCC flipcharts etc?",
                  "ZCC sendo realizado e conforme (pasta no mural da linha 'QFA e ZCC flipcharts etc?"]
    
    var cellSpacingHeight: CGFloat = 60
    
    @IBAction func shareLpa(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail() {
            let emailVC = MFMailComposeViewController()
            emailVC.mailComposeDelegate = self
            emailVC.setToRecipients(["fcanaparro@android-ind.com"])
            emailVC.setMessageBody("LPA PCL&Floor", isHTML:  false)
            emailVC.setSubject("Email from LPA App")
            emailVC.setCcRecipients(["lpagravatai@gmail.com"])
            //experiment//
            let image =  generateLPAImage()
            let imageString = returnEmailStringBase64EncodedImage(image: image)
            let emailBody = "<img src='data:image/png;base64,\(imageString)' width='\(image.size.width)' height='\(image.size.height)'>"
            //experiment
            emailVC.setMessageBody(emailBody, isHTML: true)
            present(emailVC, animated: true)
        } else {
            let ac = UIActivityViewController(activityItems: [generateLPAImage()], applicationActivities: nil)
            ac.completionWithItemsHandler = { activity, success, items, error in
            }
            let popOver = ac.popoverPresentationController
            popOver?.sourceView  = view
            popOver?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popOver?.permittedArrowDirections = UIPopoverArrowDirection.any
            present(ac, animated: true, completion: nil)
            
        }
    }
    //experiment//
    func returnEmailStringBase64EncodedImage(image:UIImage) -> String {
        let imgData:NSData = UIImagePNGRepresentation(image)! as NSData;
        let dataString = imgData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        return dataString
    }
    //experiment//
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionsTableVIew.delegate=self
        questionsTableVIew.dataSource=self
        afqTableView.delegate=self
        afqTableView.dataSource=self
        
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Ajuste de teclado//
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrol.contentInset = UIEdgeInsets.zero
        } else {
            scrol.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        scrol.scrollIndicatorInsets = scrol.contentInset
        
    }
    
    
    
    
    
    //Funcoes da tabela de perguntas\\\\
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView==self.afqTableView{
            return VFQArray.count
        }else{
            return questionsArray.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView==self.afqTableView{
            cellSpacingHeight = 45
        }else{

                cellSpacingHeight = 80
        }
        return cellSpacingHeight
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView==self.afqTableView{
            let cell=afqTableView.dequeueReusableCell(withIdentifier: "treinaCell3")
            cell?.textLabel?.text = VFQArray[indexPath.row]
            cell?.textLabel?.numberOfLines=2
            cell?.backgroundColor = UIColor.white
            cell?.layer.borderColor = UIColor.black.cgColor
            cell?.layer.borderWidth = 1
            cell?.layer.cornerRadius = 8
            cell?.clipsToBounds = true
            return cell!
        }else{
            let cell=questionsTableVIew.dequeueReusableCell(withIdentifier: "cell")
            cell?.textLabel?.text = questionsArray[indexPath.row]
            cell?.textLabel?.numberOfLines=6
            cell?.backgroundColor = UIColor.white
            cell?.layer.borderColor = UIColor.black.cgColor
            cell?.layer.borderWidth = 1
            cell?.layer.cornerRadius = 8
            cell?.clipsToBounds = true
            //var cellSpacingHeight: CGFloat = 60
            return cell!
        }
    }
    
    
    
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView==self.questionsTableVIew{
            return cellSpacingHeight
            
        }
        else{        return cellSpacingHeight
        }
    }
    
    
    @IBAction func resetLayout(_ sender: Any) {
        //Iniciliza o textfield//
        InicializaTexto(element: textDataPA, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textDataSA, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textNomePA, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textNomeSA, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textLinha, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textEstacao, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textInputUm, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textInputDois, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textInputTres, text: "", delegate: LpaTxtDelegate)
        InicializaTexto(element: textInputQuatro, text: "", delegate: LpaTxtDelegate)
        
        switchSeg.isOn=true
        switchPrim.isOn=true
        primNivelRespUm.isEnabled=true
        primNivelRespDois.isEnabled=true
        primNivelRespTres.isEnabled=true
        primNivelRespQuatro.isEnabled=true
        primNivelRespCinco.isEnabled=true
        primNivelRespSeis.isEnabled=true
        primNivelRespSete.isEnabled=true
        primNivelRespOito.isEnabled=true
        PrimNivelRespNove.isEnabled=true
        primNivelRespDez.isEnabled=true
        segNivelRespUm.isEnabled=true
        segNivelRespDois.isEnabled=true
        segNivelRespTres.isEnabled=true
        segNivelRespQuatro.isEnabled=true
        segNivelRespCinco.isEnabled=true
        segNivelRespSeis.isEnabled=true
        segNivelRespSete.isEnabled=true
        segNivelRespOito.isEnabled=true
        segNivelRespNove.isEnabled=true
        segNivelRespDez.isEnabled=true
        segNivelRespOnze.isEnabled=true
        primNivelRespOnze.isEnabled=true
        primNivelRespDoze.isEnabled=true
        segNivelRespDoze.isEnabled=true
        primNivelRespUm.selectedSegmentIndex = -1
        primNivelRespDois.selectedSegmentIndex = -1
        primNivelRespTres.selectedSegmentIndex = -1
        primNivelRespQuatro.selectedSegmentIndex = -1
        primNivelRespCinco.selectedSegmentIndex = -1
        primNivelRespSeis.selectedSegmentIndex = -1
        primNivelRespSete.selectedSegmentIndex = -1
        primNivelRespOito.selectedSegmentIndex = -1
        PrimNivelRespNove.selectedSegmentIndex = -1
        primNivelRespDez.selectedSegmentIndex = -1
        segNivelRespUm.selectedSegmentIndex = -1
        segNivelRespDois.selectedSegmentIndex = -1
        segNivelRespTres.selectedSegmentIndex = -1
        segNivelRespQuatro.selectedSegmentIndex = -1
        segNivelRespCinco.selectedSegmentIndex = -1
        segNivelRespSeis.selectedSegmentIndex = -1
        segNivelRespSete.selectedSegmentIndex = -1
        segNivelRespOito.selectedSegmentIndex = -1
        segNivelRespNove.selectedSegmentIndex = -1
        segNivelRespDez.selectedSegmentIndex = -1
        segNivelRespOnze.selectedSegmentIndex = -1
        primNivelRespOnze.selectedSegmentIndex = -1
        primNivelRespDoze.selectedSegmentIndex = -1
        segNivelRespDoze.selectedSegmentIndex = -1
        primNivelRespUm.tintColor=self.view.tintColor
        primNivelRespDois.tintColor=self.view.tintColor
        primNivelRespTres.tintColor=self.view.tintColor
        primNivelRespQuatro.tintColor=self.view.tintColor
        primNivelRespCinco.tintColor=self.view.tintColor
        primNivelRespSeis.tintColor=self.view.tintColor
        primNivelRespSete.tintColor=self.view.tintColor
        primNivelRespOito.tintColor=self.view.tintColor
        PrimNivelRespNove.tintColor=self.view.tintColor
        primNivelRespDez.tintColor=self.view.tintColor
        segNivelRespUm.tintColor=self.view.tintColor
        segNivelRespDois.tintColor=self.view.tintColor
        segNivelRespTres.tintColor=self.view.tintColor
        segNivelRespQuatro.tintColor=self.view.tintColor
        segNivelRespCinco.tintColor=self.view.tintColor
        segNivelRespSeis.tintColor=self.view.tintColor
        segNivelRespSete.tintColor=self.view.tintColor
        segNivelRespOito.tintColor=self.view.tintColor
        segNivelRespNove.tintColor=self.view.tintColor
        segNivelRespDez.tintColor=self.view.tintColor
        segNivelRespOnze.tintColor=self.view.tintColor
        primNivelRespOnze.tintColor=self.view.tintColor
        primNivelRespDoze.tintColor=self.view.tintColor
        segNivelRespDoze.tintColor=self.view.tintColor
    }
    
    @IBAction func lockPrimNivel(_ sender: UISwitch) {
        if sender.isOn==false{
            primNivelRespUm.isEnabled=false
            primNivelRespDois.isEnabled=false
            primNivelRespTres.isEnabled=false
            primNivelRespQuatro.isEnabled=false
            primNivelRespCinco.isEnabled=false
            primNivelRespSeis.isEnabled=false
            primNivelRespSete.isEnabled=false
            primNivelRespOito.isEnabled=false
            PrimNivelRespNove.isEnabled=false
            primNivelRespDez.isEnabled=false
            primNivelRespOnze.isEnabled=false
            primNivelRespDoze.isEnabled=false
            
        }
        else
        {
            primNivelRespUm.isEnabled=true
            primNivelRespDois.isEnabled=true
            primNivelRespTres.isEnabled=true
            primNivelRespQuatro.isEnabled=true
            primNivelRespCinco.isEnabled=true
            primNivelRespSeis.isEnabled=true
            primNivelRespSete.isEnabled=true
            primNivelRespOito.isEnabled=true
            PrimNivelRespNove.isEnabled=true
            primNivelRespDez.isEnabled=true
            primNivelRespOnze.isEnabled=true
            primNivelRespDoze.isEnabled=true
            
        }
    }
    
    @IBAction func lockSegNivel(_ sender: UISwitch) {
        if sender.isOn==false{
            segNivelRespUm.isEnabled=false
            segNivelRespDois.isEnabled=false
            segNivelRespTres.isEnabled=false
            segNivelRespQuatro.isEnabled=false
            segNivelRespCinco.isEnabled=false
            segNivelRespSeis.isEnabled=false
            segNivelRespSete.isEnabled=false
            segNivelRespOito.isEnabled=false
            segNivelRespNove.isEnabled=false
            segNivelRespDez.isEnabled=false
            segNivelRespOnze.isEnabled=false
            segNivelRespDoze.isEnabled=false
        }
        else
        {
            segNivelRespUm.isEnabled=true
            segNivelRespDois.isEnabled=true
            segNivelRespTres.isEnabled=true
            segNivelRespQuatro.isEnabled=true
            segNivelRespCinco.isEnabled=true
            segNivelRespSeis.isEnabled=true
            segNivelRespSete.isEnabled=true
            segNivelRespOito.isEnabled=true
            segNivelRespNove.isEnabled=true
            segNivelRespDez.isEnabled=true
            segNivelRespOnze.isEnabled=true
            segNivelRespDoze.isEnabled=true
        }
    }
    
    ///FUNCOES DE FORMATACAO, FUNCOES AUXILIARES E METODOS CONSTRUTORES
    
    
    
    
    @IBAction func editTextNomePA(_ sender: Any) {
        textDataPA.text=hoje()
    }
    
    @IBAction func editTextNomeSA(_ sender: Any) {
        textDataSA.text=hoje()
    }
    
    func generateLPAImage() -> UIImage {
        // Render view to an image
        UIGraphicsBeginImageContext(self.view.frame.size)
        view.drawHierarchy(in: self.view.frame, afterScreenUpdates: true)
        let LPAImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return LPAImage
    }
    
    //CONSTRUTOR DO INICIALIZA TEXTFIELD DO CABECALHO
    func InicializaTexto(element: UITextField, text: String, delegate: UITextFieldDelegate) {
        element.text = text
        element.delegate = delegate
        element.textAlignment = NSTextAlignment.left
        element.isHidden = false
    }
    
    
    //FUNCAO PARA OBTER A DATA DO DIA EM STRING
    func hoje () -> String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: Date())
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "dd-MMM-yyyy"
        // again convert your date to string
        let myStringtxt = formatter.string(from: yourDate!)
        return(myStringtxt)
    }
    ///////////////////// ACOES DOS BOTOES DE SEGMENTO
    
    
    @IBAction func btnPrimNvlUm(_ sender: Any) {
        if primNivelRespUm.selectedSegmentIndex==1{
            primNivelRespUm.tintColor=UIColor.red
        }else{
            primNivelRespUm.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlDois(_ sender: Any) {
        if primNivelRespDois.selectedSegmentIndex==1{
            primNivelRespDois.tintColor=UIColor.red
        }else{
            primNivelRespDois.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlTres(_ sender: Any) {
        if primNivelRespTres.selectedSegmentIndex==1{
            primNivelRespTres.tintColor=UIColor.red
        }else{
            primNivelRespTres.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlQuatro(_ sender: Any) {
        if primNivelRespQuatro.selectedSegmentIndex==1{
            primNivelRespQuatro.tintColor=UIColor.red
        }else{
            primNivelRespQuatro.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlCinco(_ sender: Any) {
        if primNivelRespCinco.selectedSegmentIndex==1{
            primNivelRespCinco.tintColor=UIColor.red
        }else{
            primNivelRespCinco.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlSeis(_ sender: Any) {
        if primNivelRespSeis.selectedSegmentIndex==1{
            primNivelRespSeis.tintColor=UIColor.red
        }else{
            primNivelRespSeis.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlSete(_ sender: Any) {
        if primNivelRespSete.selectedSegmentIndex==1{
            primNivelRespSete.tintColor=UIColor.red
        }else{
            primNivelRespSete.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlOito(_ sender: Any) {
        if primNivelRespOito.selectedSegmentIndex==1{
            primNivelRespOito.tintColor=UIColor.red
        }else{
            primNivelRespOito.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlNove(_ sender: Any) {
        if PrimNivelRespNove.selectedSegmentIndex==1{
            PrimNivelRespNove.tintColor=UIColor.red
        }else{
            PrimNivelRespNove.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlDez(_ sender: Any) {
        if primNivelRespDez.selectedSegmentIndex==1{
            primNivelRespDez.tintColor=UIColor.red
        }else{
            primNivelRespDez.tintColor=UIColor.blue
        }
    }
    @IBAction func btnSegNvlUm(_ sender: Any) {
        if segNivelRespUm.selectedSegmentIndex==1{
            segNivelRespUm.tintColor=UIColor.red
        }else{
            segNivelRespUm.tintColor=UIColor.blue
        }
        
    }
    @IBAction func btnSegNvlDois(_ sender: Any) {
        if segNivelRespDois.selectedSegmentIndex==1{
            segNivelRespDois.tintColor=UIColor.red
        }else{
            segNivelRespDois.tintColor=UIColor.blue
        }
    }
    @IBAction func btnSegNvlTres(_ sender: Any) {
        if segNivelRespTres.selectedSegmentIndex==1{
            segNivelRespTres.tintColor=UIColor.red
        }else{
            segNivelRespTres.tintColor=UIColor.blue
        }
    }
    @IBAction func btnSegNvlQuatro(_ sender: Any) {
        if segNivelRespQuatro.selectedSegmentIndex==1{
            segNivelRespQuatro.tintColor=UIColor.red
        }else{
            segNivelRespQuatro.tintColor=UIColor.blue
        }
    }
    @IBAction func btnSegNvlCinco(_ sender: Any) {
        if segNivelRespCinco.selectedSegmentIndex==1{
            segNivelRespCinco.tintColor=UIColor.red
        }else{
            segNivelRespCinco.tintColor=UIColor.blue
        }
    }
    @IBAction func btnSegNvlSeis(_ sender: Any) {
        if segNivelRespSeis.selectedSegmentIndex==1{
            segNivelRespSeis.tintColor=UIColor.red
        }else{
            segNivelRespSeis.tintColor=UIColor.blue
        }
    }
    @IBAction func btnSegNvlSete(_ sender: Any) {
        if segNivelRespSete.selectedSegmentIndex==1{
            segNivelRespSete.tintColor=UIColor.red
        }else{
            segNivelRespSete.tintColor=UIColor.blue
        }
    }
    
    @IBAction func btnSegNvlOito(_ sender: Any) {
        if segNivelRespOito.selectedSegmentIndex==1{
            segNivelRespOito.tintColor=UIColor.red
        }else{
            segNivelRespOito.tintColor=UIColor.blue
        }
    }
    
    @IBAction func btnSegNvlNove(_ sender: Any) {
        if segNivelRespNove.selectedSegmentIndex==1{
            segNivelRespNove.tintColor=UIColor.red
        }else{
            segNivelRespNove.tintColor=UIColor.blue
        }
    }
    
    @IBAction func btnSegNvlDez(_ sender: Any) {
        if segNivelRespDez.selectedSegmentIndex==1{
            segNivelRespDez.tintColor=UIColor.red
        }else{
            segNivelRespDez.tintColor=UIColor.blue
        }
        
    }
    
    @IBAction func btnPrimNvlOnze(_ sender: Any) {
        if primNivelRespOnze.selectedSegmentIndex==1{
            primNivelRespOnze.tintColor=UIColor.red
        }else{
            primNivelRespOnze.tintColor=UIColor.blue
        }
    }
    
    @IBAction func btnSegNvlOnze(_ sender: Any) {
        if segNivelRespOnze.selectedSegmentIndex==1{
            segNivelRespOnze.tintColor=UIColor.red
        }else{
            segNivelRespOnze.tintColor=UIColor.blue
        }
    }
    @IBAction func btnPrimNvlDoze(_ sender: Any) {
        if primNivelRespDoze.selectedSegmentIndex==1{
            primNivelRespDoze.tintColor=UIColor.red
        }else{
            primNivelRespDoze.tintColor=UIColor.blue
        }
    }
    @IBAction func btnSegNvlDoze(_ sender: Any) {
        if segNivelRespDoze.selectedSegmentIndex==1{
            segNivelRespDoze.tintColor=UIColor.red
        }else{
            segNivelRespDoze.tintColor=UIColor.blue
        }
    }
    
}

